\documentclass[letterpaper,12pt]{article}
\usepackage{fancyhdr}
\usepackage[T1]{fontenc}
\usepackage[margin=1in]{geometry}
% \usepackage{hyperref}
\usepackage{lipsum}
\usepackage{lmodern}
\usepackage{mathptmx}
\usepackage{sepfootnotes}
\usepackage[doublespacing]{setspace}
\usepackage{textcomp}

\title{The Validity of International Law as a Domain of Law\\{\Large{}LAW 707:
    International Law}}
\author{N. Mertin}

\pagestyle{fancy}
\fancyhf{}

\begin{document}
\maketitle
\thispagestyle{empty}
\newpage
\fancyhead[R]{\thepage}

\sepfootnotecontent{kantorowicz}{\label{kantorowicz}Hermann Kantorowicz,
  \textit{The Definition of Law}, ed by AH Campbell (Cambridge, UK:\ Cambridge
  University Press, 1958).}
\sepfootnotecontent{black}{\label{black}See Max Black, ``The Gap Between `Is'
  and `Should'\,'' (1964) 73:2 Philosophical Rev 165 (logical implication of
  Hume's Guillotine at 166).}
\sepfootnotecontent{cotterrell}{\label{cotterrell}See e.g.\ Roger Cotterrell,
  ``The Sociological Concept of Law'' (1983) 10:2 JL \& Soc'y 241 (discussion
  and comparison various conceptual definitions of `law' for the purpose of
  sociology); Kantorowicz, \textit{supra} note~\ref{kantorowicz} at 21.}
\sepfootnotecontent{charter}{\label{charter}Part I of the \textit{Constitution
    Act, 1982}, being Schedule B to the \textit{Canada Act 1982} (UK), 1982, c
  11.}
\sepfootnotecontent{mackie}{\label{mackie}See also Sarah Mackie, ``Brexit and
  the Trouble with an Uncodified Constitution: \textit{R (Miller) v Secretary of
    State for Exiting the European Union}'' (2017) 42:2 Vermont L Rev (the
  practicalities of an uncodified constitution at 300--04).}
\sepfootnotecontent{constitution}{\label{constitution}See e.g.\
  \textit{Constitution Act, 1867} (UK), 30 \& 31 Vict, c 3, s 92, reprinted in
  RSC 1985, Appendix II, No 5.}
\sepfootnotecontent{anaya}{\label{anaya}See e.g.\ S James Anaya, ``Report of the
  Special Rapporteur on the Rights of Indigenous Peoples in the Situation of
  Indigenous Peoples in Canada'' (2015) 32:1 Ariz J Intl \& Comp L 143 at
  154--59.}
\sepfootnotecontent{glenn}{\label{glenn}See also Patrick H Glenn, ``Reconciling
  Regimes: Legal Relations of States and Provinces in North America'' (1998) 15
  Ariz J Intl \& Comp L 255.}
\sepfootnotecontent{bill21}{\label{bill21}See e.g.\ Kerri A Froc, ``Shouting
  into the Constitutional Void: Section 28 and Bill 21'' (2019) 28:4 Const Forum
  Const 19; \textit{Act respecting the laicity of the State}, CQLR c L-0.3.}
\sepfootnotecontent{disallowance}{\label{disallowance}\textit{Cf} Robert C
  Vipond, ``Constitutional Politics and the Legacy of the Provincial Rights
  Movement in Canada'' (1985) 18:2 Can J Political Science 267. See also Harlow
  J Heneman, ``Dominion Disallowance of Provincial Legislation in Canada''
  (1937) 31:1 American Political Science Rev 92 (historical contemporary view on
  the disallowance power); \textit{Constitution Act, 1867}, \textit{supra}
  note~\ref{constitution} s 56.}
\sepfootnotecontent{hansas}{\label{hansas}See John Hansas, ``Hayek, the Common
  Law, and Fluid Drive'' (2004) 1 New York U JL \& Liberty 79 (origins of the
  English legal system at 89--90).}
\sepfootnotecontent{vclt}{\label{vclt}23 May 1969, 1155 UNTS 331 (entered into
  force 27 January 1980).}
\sepfootnotecontent{montevideo}{\label{montevideo}\textit{Montevideo Convention
    on the Rights and Duties of States}, 26 December 1933, 165 LNTS 20 (entered
  into force 26 December 1934).}

\section{Introduction}

International law is necessarily included in any definition of ``law'' which is
useful for formalizing the science of law and general jurisprudence. This is
demonstrable through the rigorous, analytical process of constructing and
deducing a definition which codifies what is customarily considered law in a way
which is logically consistent, useful for legal analysis and otherwise minimally
restrictive. This process follows the reasoning of Kantorowicz's \textit{The
  Definition of Law}\sepfootnote{kantorowicz} while taking an interdisciplinary
approach which draws support from ideas and insights used to address similar
definitional issues in philosophy and constructive mathematics.

This paper begins by discussing how an abstract concept such as ``law'' may be
defined, and then iteratively develops a definition based on that discussion. A
brief digression discusses the role which sources of law and the property of
being ``in force'' plays in delimiting a definition of law. Finally, public
international law is evaluated against the standard developed in the earlier
sections, and implications and corollaries of the result are discussed.

\section{An Approach to Defining Law}

The question to which the above thesis proposes an answer is: ``is international
law really `law'?'' Fundamentally, a definition of the word ``law'' is a
necessary context in order to give any answer to the question. However, the
question of a definition is itself far from trivial.

\subsection{The Question}

Kantorowicz devotes considerable literary real estate to the question of what it
means to usefully define an abstract concept such as law, writing that ``any
question posed by any science as to the meaning of a term can be answered only
if the intention is to ask what in \textit{this} particular science
\textit{ought} to be understood by this particular
term.''\footnote{\textit{Ibid} at 5 (emphasis in original).}

Therefore it would be pertinent to adjust the fundamental question to ``ought we
to consider international law as `law'\,'', which naturally depends on the
question of ``what ought to be understood by `law' in this
context''.\sepfootnote{black} There are infinitely many valid
definitions;\sepfootnote{cotterrell} choosing the correct one depends on the
requirements of the present context.\footnote{\textit{Cf} Kantorowicz,
  \textit{supra} note~\ref{kantorowicz} at 4.} The purpose for which a
definition is sought this paper are assumed to be the application of the
conventional analytical techniques and tools of law to the subject matter and
the differentiation of law from other types of customs or rules.

\subsection{The Requirements for a Definition}

A definition of `law' which is valid for the purposes of this paper must include
all that which is customarily considered to be law, such as criminal law, family
law, constitutional law, and tort law. It must also be logically consistent and
unambiguous; in other words, there cannot be any subject matter which one may
conclude as being both definitively law and definitively not law through
different valid uses of the definition. Finally, it must be valid across time,
meaning that it must include, to the extent reasonably possible, all which is,
has been and could in the future be law.

These requirements ensure that the definition is usable for objective logical
analysis of legal issues. Inclusion of conventional domains of law ensures the
sanity and usefulness of the proposed definition, logical consistency is
inherently necessary for any logical applicability, and time invariance ensures
that the definition is not ``over-fitted'' to the present state of law. With
these requirements as a basis, a definition will be developed which describes
precisely that to which the conventional analytical tools and techniques of law
are applicable.

\subsection{The Form of a Definition}

The choice of a process for creating a definition for a term---and the resulting
form of the definition---can be a means by which significant bias, intentionally
or otherwise, is imparted on the result. It is easy for an author to state a
predetermined answer and justify that it is valid as a definition; however, this
does not make for the sort of minimally restrictive definition which describes
precisely that which is useful to consider as law. Therefore, a process which
removes some hidden decision-making and ensures that all constraints placed on
the definition are discussed and justified is more suitable to this task.

To that end, an axiomatic approach is proposed. It is common in constructive
mathematics to give a definition of an abstract concept in parts, starting
initially with a minimal, constructive definition giving the form of the
concept, followed by any number of criteria, called axioms, which constrain what
things meet the definition; it is often the axioms which differ between
alternative definitions. This approach allows for an iterative process to refine
a definition, by independently evaluating proposed axioms for consistency with
customary usage of the term being defined and necessity for the use of the
definition. This approach also helps to achieve a minimally restrictive
definition, by starting from a broad foundation and gradually narrowing
according to the needs of the situation.

\section{A Definition}

Following the procedure described above, a starting point for a definition can
be proposed. Choosing as a starting point a particular category of things to
which it is claimed that all ``law'' belongs requires a review of the intuition
and customary treatment of law. To this end, it is instructive to consider the
range of agreed-upon domains of law---such as criminal law, family law,
constitutional law, and tort law---as well as the manner in which law is
analyzed and discussed, in order to reveal any common structure.

Such an analysis will quickly recognize that law of any sort consists of---or in
any event, may be equivalently represented as---a set of rules which impose
restrictions or expectations on human behaviour. For example, a rule of criminal
law may prohibit the killing of another person, save for certain self-defence
provisions, while a rule of family law may require parents to provide certain
forms of financial support to their children. However, any thing which one
considers to be part of law, under any definition of law compatible with the
application of legal analysis and other legal techniques, must take the form of
a rule regarding the behaviour of humans.

It may be argued that the above proposition ignores the possibility of law which
applies to legal entities such as corporations or governments, or law which is
described as creating rights rather than restricting behaviour; however, both of
these categories can be shown to be included in the above proposition. Law
governing entities set up by people is substantively equivalent to law binding
on the people ultimately in control of the entities; the concept of the entity
is merely a convenient tool for describing a class of rules binding on its human
controllers. By a similar argument, law which purports to create rights, such as
the \textit{Canadian Charter of Rights and Freedoms}\sepfootnote{charter},
necessarily does so by imposing a restriction on the ability of the state---an
entity created and operated by people---or other people to take action which
infringes on those rights. This idea of the substantive equivalence is a useful
tool to ensure that concepts are assessed and studied on the basis of substance
rather than the linguistic form in which they appear.

The following subsections will discuss the potential axioms which may be added
to the base definition constructed here, arriving at a complete definition
satisfying the requirements set out above. Various potential axioms will be
eliminated by showing that they are incompatible with the preceding
requirements.

\subsection{Codification}

Must law be codified, or in other words, provided in a centralized, written form
by a law-making authority? While codification is certainly useful for analysis
of law, it cannot be regarded as necessary, for doing so would exclude a variety
of legitimate domains of law that must be included in the definition, ranging
from the judge-made common law to the constitutional foundations of the United
Kingdom and its predecessors.\sepfootnote{mackie} Therefore, inclusion of real
law precludes the inclusion of a codification axiom in our definition.

\subsection{Predictability}

Must law be sufficiently understandable as to be predictable by someone subject
to it? This proposed axiom confuses form with substance; the source of a legal
rule, be it in text, custom or elsewhere, and the form and clarity of that
source, is distinct from the content of the rule.

Moreover, such an axiom assumes certain competences of the subjects of the law
in order for them to be able to interpret it. This leads to ambiguities and
issues with our intuitive understanding of what constitutes law: if the majority
of the population does not speak the language in which a law is published---or
speaks it but is illiterate, or, as is the case in many modern societies,
requires the assistance of a professional lawyer to understand it---is it still
``law''? Evidently, such dissection is an irrelevant distraction from the quest
for a constructive definition of law, and any sort of predictability axiom is
inadmissible to our definition.

\subsection{Enforcement}

Must law be enforced in order to be law? There are many issues with such an
axiom. Firstly, enforcement itself is a subjective and often inexact concept;
for example, does ``enforced'' mean that the law is always obeyed, or does it
mean that detractors are punished? The former, as will be shown in the next
section, is inadmissible; for the latter, what level and consistency of
punishment or proportion of the population obeying is sufficient?

In general, involving a quantitative component (e.g., ``what level is
sufficient?'') in a definitional question invites ambiguity and significantly
limits the usefulness and clarity of the proposed definition. Further, even if
one were to include this axiom, it would exclude many legal systems where
detractors often go unpunished or minimally punished. A prime example is
constitutional provisions binding on governments, enforcement of which often
either fail due to the legal authority of the government enabling it to thwart
enforcement in practice, or are wholly dependent on the government itself taking
positive action to comply;\sepfootnote{anaya} an even more egregious example is
the criminal prohibition of drugs. Clearly, an axiom mandating law to be
enforced is incompatible with the requirement that existing law be included in
our definition.

\subsection{Violability}

Based on the above reasoning, it is clear that not all which is ``law'' is
(always) enforced, and consequently not all which is law is (always) obeyed.
This raises a natural question: does there exist any ``law'' which is always
obeyed? Or, on the contrary, must all ``law'' be violable?

Analysis shows the latter case to be true. In particular, the ``tools and
techniques of legal analysis'' which the definition aims to support presuppose
that law encodes what is strictly not necessarily true but, in some
philosophical context, ought to be; rules which merely describe what is
necessarily (or statistically) true---the so-called ``laws'' described by the
natural sciences---cannot, for the purposes of this definition, constitute
``law''. This exclusion is necessary in order for wide ranges of legal study to
be applicable, from analysis of the sources and historical evolution of law to
discussion of what the law ought to be and its connections with politics; a law
which is a static, inviolable property of nature precludes any legal analysis of
the sort. Therefore, not only is it acceptable that a rule is violable in order
to be considered law, it is necessary; this constitutes an axiom in our
definition.\footnote{See also Kantorowicz, \textit{supra} note~\ref{kantorowicz}
  at 25.}

\subsection{Central Authority}

Similarly to enforcement, it may be proposed that a rule must flow from a
central authority---often the State---in order to be considered law. While this
idea is often a tool for criticizing public international law for its
decentralized nature and the lack of such an authority, it is often overlooked
that the relationship between States under public international law is not
entirely different from the relationship between sub-units of a federal
nation-state such as Canada.\sepfootnote{glenn} Crucially, one should not
confuse the existence of a federal level of government with the existence of a
central law-giving authority; the federal level of government does not, at law,
have absolute legal authority over the provinces.\sepfootnote{constitution} It
is indeed this distinction which distinguishes federations from unitary or
devolved states.

As a practical consequence of the legal autonomy of the provinces, it is
possible for one to take action which may be viewed as disobeying the legal
order between them (i.e., being \textit{ultra vires} the authority of the
provincial legislature);\sepfootnote{bill21} the \textit{de facto} relationships
between the provinces mean that the assumption that the constitutional legal
order between them will be obeyed is more grounded in faith in the rule of law
than in the hard power associated with the idea of a central authority.
Moreover, The constitutional legal order is shaped collaboratively and often
implicitly, as with the disallowance power,\sepfootnote{disallowance} by the
parties involved, rather than strictly flowing out from the centre. Thus, an
axiom requiring a central authority for a domain of law would inappropriately
exclude the constitutional legal orders of federations from the study of law,
and therefore, such an axiom is inadmissible to our definition.

\subsection{Sufficiency for the Purpose of the Definition}

Is the definition created so far sufficient with respect to its purpose, namely,
the application of the conventional analytical techniques and tools of
law?\footnote{See Section 2 for discussion.} From the definition as it stands
(the basic form of a body of rules on human behaviour and the one axiom
concerning violability), it is deducible that law describes rules governing the
behaviour of people which are not physically guaranteed or self-fulfilling, but
rather expected of the people by their peers. Thus anything which meets this
definition is a legitimate subject of the tools of law which study its sources,
societal implications, and other attributes\footnote{\textit{Cf} Kantorowicz,
  \textit{supra} note~\ref{kantorowicz} at 13ff.}

\section{Sources of Law}\label{sec:in-force}

The preceding section constructed a definition for the subject matter to be
considered as ``law''. This section discusses the connection to the evaluation
of sources of law to determine what law, in a given jurisdictional and temporal
context, is ``in force''.

\subsection{Definitional Relevance}

Must all law be in force? Certainly in many contexts, when one refers to law (or
often ``\textit{the} law''), one is referring implicitly to the laws which are
currently in force. However, that intuitiveness alone is insufficient to justify
a corresponding axiom in the definition of law; in doing so, two main problems
arise. Firstly, such an axiom would exclude, seemingly arbitrarily, the study of
any historical or hypothetical laws, which would make much analysis rather
futile. Secondly, due to the complex and often conflicting nature of legal
authorities---especially in times and places of crisis and political
uncertainty---it may not always be possible to ensure a logically consistent
model of sources of law which reflects the real world. Thus, while analysis of
the sources of law is certainly useful and important in the study of law, it
cannot be regarded as part of the definition of the subject matter.

\subsection{Conditions for Being In Force}

Determining what law is and is not in force is far from trivial. In a given
context, one might be able to describe the conditions or process for creating
legislation---such as the process for passing an Act of Parliament, or the means
and restrictions under which the President of the United states may create law
using an Executive Order---but this always presupposes the legitimacy of a more
fundamental level of law, in these cases the Constitutions of Canada and the
United States, respectively. So, from a blank slate, how might one determine
what law is in force?

While in many cases law is enacted pursuant to the authority granted under
another, existing piece of law, that cannot be universally true, as it would
recursively require an infinite chain of legal authority. Indeed, while acts of
Parliament in Canada derive their authority from the \textit{Constitution Act,
  1867},\footnote{\textit{Supra} note~\ref{constitution}.} which in turn derives
its authority from its enacting by the Imperial Parliament, that chain must end
somewhere somewhere. Regardless what path of legal justification one chooses, it
inevitably is based on some moral, religious or similarly customary basis, which
is viewed as having force merely because it is sufficiently widely agreed upon.

Therefore, as a corollary to the thesis of this paper, the following is
proposed: that all law which is in force is fundamentally customary, i.e., it
derives its authority, directly or indirectly, from the custom of the people
bound by it; not only is it acceptable for law to be customary, it is
unavoidable. Building on this observation, the following definition is proposed
for law which is in force: a legal rule is in force if and only if it is either
\textit{intra vires} subordinate legislation enacted pursuant to another legal
rule which is in force, or is customarily accepted by its subjects. Custom thus
provides an escape hatch from the recursion.

\section{Evaluating International Law}

On the basis of the above observations, attention will now be turned to public
international law. Substantively, it fits into the mould of ``rules binding on
people'', as it binds on sovereigns and their officials. It indisputably is
violable, and thus meets the requirements of the only axiom included in the
definition. Therefore, per our definition, it may be legitimately regarded as
law.

It is worth addressing the visible difference in form between public
international law and the domestic legal systems of many modern jurisdictions:
namely, that international law is significantly based on evolving customary
norms rather than codified, written law. While, per the preceding sections, this
in no way eliminates it from inclusion in a definition of law, additional
support for this point may be found by looking at the historical evolution of
those domestic legal systems. Most notably, English common law, on which many of
the world's legal systems are based, is itself built on a gradual evolution from
a purely customary legal system over the early centuries of the modern English
state.\sepfootnote{hansas} As it evolved, purely customary rules were gradually
replaced by codified versions which restated and encoded them. Arguably, much
the same is gradually occurring in the sphere of international law, be it treaty
law in the \textit{Vienna Convention on the Law of Treaties},\sepfootnote{vclt}
or the definition of States in the \textit{Montevideo
  Convention}.\sepfootnote{montevideo} Thus, not only is constitutional law
acceptable under the definition constructed in this paper, it also broadly
resembles early forms of some domestic legal systems.

\section{Conclusion}

A constructive and axiomatic definition of ``law'' provides a minimally
restrictive definition of that which can be analyzed with the tools and
techniques of law and jurisprudence. Such a definition which is not arbitrarily
restricted necessarily concludes that international law as a subject matter is
included in ``law'', and customary international law as its fundamental source
is a valid means of constituting law which is in force.

\end{document}
