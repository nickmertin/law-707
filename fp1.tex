\documentclass[letterpaper,12pt]{article}
\usepackage{cleveref}
\usepackage{fancyhdr}
\usepackage[margin=1in]{geometry}
\usepackage{lipsum}
\usepackage{mathptmx}
\usepackage[doublespacing]{setspace}
\usepackage{siunitx}

\title{The Validity of \textit{Riddle et al.\ v.\ Dumbledore} under
  International Law\\{\Large{}LAW 707: International Law}}
\author{N. Mertin}

\pagestyle{fancy}
\fancyhf{}

% \crefformat{footnote}{#2\footnotemark[#1]#3}

\begin{document}
\maketitle
\thispagestyle{empty}
\newpage
\fancyhead[R]{\thepage}

\section{Introduction}
Albus Dumbledore, the former Prime Minister of the State of Hogwarts, was served
with a Statement of Claim for a civil suit in the Ontario Superior Court of
Justice regarding alleged torture committed while he was in office. He was in
the province on vacation at the time when he was served the claim. The claims in
the suit allege liability for ordering the torture of the plaintiff, Thomas
Riddle, and for cancelling a construction contract with the plaintiff's
business, which is incorporated under the laws of Luxembourg. The Court ruled
against Dumbledore \textit{in absentia} following his refusal to appear for the
proceedings, and due to the ruling, \SI{7000000}[\$]{} was seized from a
Canadian bank account belonging to Dumbledore.

Canada and Hogwarts are both sovereign States, and are both party to the
\textit{Vienna Convention on Consular
  Relations}\footnote{\label{vccr}\textit{Vienna Convention on Consular
    Relations}, 24 April 1963, 596 UNTS 261 (entered into force 19 March 1967)
  [\textit{VCCR}].} and the \textit{Convention against
  Torture}.\footnote{\label{cat}\textit{Convention against Torture and Other
    Cruel, Inhuman or Degrading Treatment or Punishment}, 10 December 1984, 1465
  UNTS 85 (entered into force 26 June 1987) [\textit{CAT}].} The issue at hand
is whether the proceedings and result are compatible with Canada's obligations
under international law.

\section{Rules}
To answer the question, the rules governing the competency of courts to claim
jurisdiction and the immunity of former State officials must be established.

\subsection{Jurisdiction}
The jurisdiction issue is concerned not with the competencies of the Ontario
Superior Court of Justice as prescribed in its constitution---that is a matter
of municipal law---but rather with the competency of any Canadian court to claim
jurisdiction over cases \textit{vis-à-vis} the international legal obligations
of the Canadian State.

Under customary international law, a State (or the courts thereof) claiming
jurisdiction over a particular matter must establish the right to do so with
respect to any of the several jurisdictional principles. The territoriality
principle in particular ``entitles a State to exercise jurisdiction over
everything and everyone found on its territory, except those that are excluded
under treaties or customary international law\ldots{} The right of a State to
exercise territorial jurisdiction is essential, and it is considered that such
right is absolute and unassailable.''\footnote{Ademola Abass, \textit{Complete
    International Law}, 2nd ed (Oxford: Oxford University Press, 2014), ch 7 at
  240.} The effect of this principle is that, subject to treaties and immunities
under international law, it is the right of a sovereign State to enforce its
laws on its territory. In addition, the universality principle covers ``certain
crimes [which transcend] the jurisdiction of any single State'' by virtue of
their impact and can in some cases override immunities.\footnote{\textit{Ibid},
  ch 7 at 251.}

\subsection{Immunity}
The \textit{Vienna Convention on Consular Relations}\footnote{\textit{Supra}
  note~\ref{vccr}.}, to which Canada is a party, provides immunity in a host
country to various officials of diplomatic missions, but not to other State
officials. Absent another treaty which does cover other State officials, the
remaining potential source of immunity is customary international law and
relevant municipal law which codifies it.

As described by the International Court of Justice in \textit{Arrest Warrant of
  11 April 2000}, ``in international law it is firmly established that, as also
diplomatic and consular agents, certain holders of high-ranking office in a
State, such as the Head of State, Head of Government and Minister of Foreign
Affairs, enjoy immunities for jurisdiction in other States, both civil and
criminal.''\footnote{Arrest Warrant of 11 April 2000 (Democratic Republic of the
  Congo v\ Belgium), [2002] ICJ Rep 3 at 21.}. In Canadian law, this subject is
covered by the \textit{State Immunity Act}.\footnote{\label{sia}\textit{State
    Immunity Act}, RSC 1985, c S-18 [\textit{SIA}].} This Act confers immunity
from legal action in Canadian courts on States and their leaders, with an
exhaustive list of exceptions.

\section{Analysis}
As Dumbledore was in Ontario at the time of the action, there is no doubt that,
under the territoriality principle, the Ontario Superior Court of Justice is
able to claim jurisdiction in the case. Therefore, the question becomes a matter
of immunities.

The case is a civil action regarding actions taken by Dumbledore in his capacity
as Prime Minister of Hogwarts. As such, the \textit{SIA} applies, and since
Dumbledore in the context of the claims is a foreign Head of Government, he is
entitled to immunity unless the matter falls under one of the exceptions
provided for in that Act.\footnote{\textit{Ibid}, ss 2, 3(1).} As confirmed in
\textit{Kazemi}\footnote{\label{kazemi}\textit{Kazemi Estate v Islamic Republic
    of Iran}, [2014] 3 SCR 176 at para 110 [\textit{Kazemi}].} and, under a
substantially set of facts to this case,
\textit{Bouzari},\footnote{\textit{Bouzari v Islamic Republic of Iran}, 2004
  CanLII 871 at para 59, [2004] OJ No 2800 (QL) [\textit{Bouzari}].} a civil
action regarding torture which occurred outside of Canada does not satisfy the
requirements for an exception under the \textit{SIA}, and therefore there under
Canadian law no avenue for recourse for Riddle through civil proceedings.

Nor is this immunity a derogation from Canada's obligations under international
law. While the \textit{CAT} requires States party to provide means for victims
of torture to obtain redress,\footnote{\textit{Supra} note~\ref{cat}, art 14.}
as confirmed by the Supreme Court of Canada in \textit{Kazemi}, this cannot be
interpreted ``as requiring Canada to implement a universal civil jurisdiction
for acts of torture.''\footnote{\textit{Supra} note~\ref{kazemi} at para 50.}

\section{Conclusion}
Under Canadian domestic law, Dumbledore is immune from civil proceedings
relating to torture committed abroad under his authority as the then-Prime
Minister of Hogwarts. This immunity is compatible with Canada's obligations
under international law. Furthermore, per the \textit{SIA}, this immunity would
not extend to criminal proceedings should they take
place.\footnote{\textit{Supra} note~\ref{sia}, s 18.}

\end{document}
