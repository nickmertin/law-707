\documentclass[letterpaper,12pt]{article}
\usepackage[style=ieee]{biblatex}
\usepackage{fancyhdr}
\usepackage[margin=1in]{geometry}
% \usepackage{hyperref}
\usepackage{lipsum}
\usepackage{mathptmx}
\usepackage[doublespacing]{setspace}

\title{The Validity of International Law as a Domain of Law\\{\Large{}LAW 707:
    International Law}}
\author{N. Mertin}

\addbibresource{Law.bib}

\pagestyle{fancy}
\fancyhf{}

\newcounter{para}

\begin{document}
\maketitle
\thispagestyle{empty}
\newpage
\fancyhead[R]{\thepage}

\section{Introduction}
International law is necessarily included in any definition of ``law'' which is
useful for formalizing the science of law and general jurisprudence. This is
demonstrable through the rigorous, analytical process of constructing and
deducing a definition which codifies what is customarily considered law in such
a way which is logically consistent, useful for analysis and otherwise minimally
restrictive. This process follows the reasoning of
Kantorowicz~\cite{kantorowicz_definition_1958} while taking an interdisciplinary
approach which draws support from ideas and insights used to address similar
definitional issues in philosophy and constructive mathematics.

\section{An Approach to Defining Law}
\begin{itemize}
\item Largely based on~\cite{kantorowicz_definition_1958}.
\item A definition of law is necessary in order to assess whether international
  law meets it.
\item Note that the question is not what are the current rules of law ``on the
  books''; rather, the question is what subject matter, whether they are in
  force or not, (would) constitute law.
\item There are infinitely many valid definitions; choosing the correct one
  depends on the use case.
\item It is not completely arbitrary; it must be compatible with customary usage
  of the term.
\end{itemize}

\subsection{The Question}
\begin{itemize}
\item ``Is international law really `law'?''
\item Per Hume's Guillotine, this should be: ``Ought we to consider
  international law as `law'?''
\item The answer depends largely on for what purposes we wish to classify some
  things as law and others as not law.
\item Kantorowicz~\cite{kantorowicz_definition_1958} works toward a definition
  useful for the scientific analysis of law as a field and domain of academic
  study.
\item Cotterrell~\cite{cotterrell_sociological_1983} describes a number of
  possible rough definitions for the purpose of sociology and the analysis of
  law in that context.
\item Our purposes are assumed to be the application of the conventional
  analytical techniques and tools of law to the subject matter, and the
  differentiation of law from other types of customs or rules.
\end{itemize}

\subsection{The Requirements for a Definition}
\begin{itemize}
\item A valid definition must include that which is customarily considered to be
  law, such as criminal law, contract law and constitutional law.
\item A valid definition must be logically consistent and unambiguous, i.e.,
  there cannot be any subject matter which one may conclude as being both
  definitively law and definitively not law through different uses of the
  definition.
\item A valid definition must be valid across time, i.e., it must include, to
  the extent reasonably possible, all which is, has been and could in the future
  be law.
\end{itemize}

\subsection{The Form of a Definition}
\begin{itemize}
\item It is common in mathematics to give a definition of an abstract concept in
  parts, with a minimal, constructive definition as an initial starting point,
  followed by any number of criteria, called axioms, which constrain what things
  meet the definition.
\item It is often the axioms which differ between alternative definitions.
\item This approach allows for an iterative process to refine a definition, by
  independently evaluating proposed axioms for consistency with customary usage
  of the term being defined and necessity for the use of the definition.
\item This approach also helps to achieve a minimally restrictive definition, by
  starting from a broad point and gradually narrowing according to the needs of
  the situation.
\end{itemize}

\section{A Definition}
Following the procedure described above, a starting point for a definition can
be proposed. Choosing as a starting point a particular category of things to
which it is claimed that all law belongs requires a review of the intuition and
customary treatment of law. To this end, it is instructive to consider the range
of agreed-upon domains of law---such as criminal law, family law, constitutional
law, and tort law---as well as the manner in which law is analyzed and
discussed, in order to reveal any common structure.

Such an analysis will quickly recognize that law of any sort consists of---or in
any event, may be wholly described by---rules which impose restrictions or
expectations on the behaviour of humans. For example, a rule of criminal law may
prohibit the killing of another human, save for certain self-defence provisions,
while a rule of family law may require parents to provide certain forms of
financial support to their children. However, any thing which one considers to
be part of law, under any definition of law compatible with the application of
legal analysis and other legal techniques, must take the form of a rule
regarding the behaviour of humans.

It may be argued that the above proposition ignores the possibility of law which
applies to legal entities such as corporations or governments, or law which is
described as creating rights rather than restricting behaviour; however, both of
these categories can be shown to be included in the above proposition. Law
governing entities set up by humans is substantively equivalent to law binding
on the humans ultimately in control of the entities; the concept of the entity
is merely a convenient tool for describing a class of rules binding on its human
controllers. By a similar argument, law which purports to create rights, such as
the \textit{Canadian Charter of Rights and Freedoms}, is in reality imposing a
restriction on the ability of the state---an entity set up by humans---to take
action which infringes on those rights. This concept of the substantive
equivalence of a particular legal concept is a useful tool to avoid categorizing
law on the basis of the linguistic form in which it
appears.\footnote{Kantorowicz~\cite{kantorowicz_definition_1958} calls this type
of blunder the fallacy of ``verbal realism'', noting that ``the most dangerous
enemy of science is that unfaithful servant and secret master of thought,
Language.''}

The following subsections will discuss the potential axioms which may be added
to the base definition constructed here, arriving at a complete definition
satisfying the requirements set out above. Various potential axioms will be
eliminated, largely via \textit{reductio ad absurdum} by showing that they are
incompatible with notions which we know to be law.

\subsection{Codification}
\begin{itemize}
\item Is there a requirement that law be codified, i.e., provided in a
  centralized, written form by a law-making authority?
\item As this would exclude the entirety of common law, the answer is clearly
  no.
\end{itemize}

\subsection{Enforcement}
\begin{itemize}
\item Is there a requirement that law be enforced?
\item There are many issues with such an axiom:
  \begin{itemize}
  \item Does ``enforced'' mean that the law is always obeyed, or does it mean
    that detractors are punished?
  \item What level of punishment or proportion of the population obeying is
    sufficient?
  \item There are many agreed-upon legal systems where detractors from the law
    often go unpunished or minimally punished; for example, conflict of interest
    laws regarding politicians, or implementation of Indigenous treaty
    rights~\cite{canada_archived_2018}.
  \end{itemize}
\item Any quantitative answer (i.e., ``what level is sufficient?'') to a
  definitional question invites ambiguity and significantly limits the
  usefulness of the purported definition.
\end{itemize}

\subsection{Predictability}
Is there a requirement that law be sufficiently understandable as to be
predictable by someone subject to it? This proposed axiom confused form with
substance; the source of a legal rule, be it in text, custom or elsewhere, and
the form and clarity of that source, is distinct from the content of the rule.
Moreover, such a requirement is subjectively dependent on the mind of the
subject, and thus is ironically ambiguous and inconsistent itself.

\subsection{Violability}
\begin{itemize}
\item The difference between a legal rule and a rule of the natural sciences.
\item Law exists and is created by humans because it would not be followed
  otherwise.
\item Natural sciences often produce empirical rules which are called ``laws'',
  but these describe phenomena that occur and are obeyed regardless of their
  perception or study by humans.
\item Furthermore, a key feature of many domains of law is the existence of
  provisions for what should occur should the law be disobeyed.
\item The exclusion of the laws of science from the definition of law is
  necessary in order for wide ranges of legal study to be applicable, from
  analysis of the sources and historical evolution of law to discussion of what
  the law ought to be and connections with politics.
\item Therefore, not only is it acceptable that a rule is violable in order to
  be considered law, it is necessary.
\end{itemize}

\subsection{Central Authority}
\begin{itemize}
\item Related to the discussion of enforcement, it may be proposed that a rule
  must flow from a central authority (often the state) in order to be considered
  law.
\item While this idea is often a tool for criticizing public international law
  for its decentralized nature and the lack of such an authority, it is often
  overlooked that the relationship between states under public international law
  is not entirely different from the relationship between sub-units of a federal
  nation-state such as Canada.
\item One should not confuse the (partial) codification of the Canadian
  constitution with the existence of a central authority; the federal level of
  government does not have unlimited authority over the provinces.
\item It is possible for one of the entities to take action which may be viewed
  as disobeying the legal order between them (i.e., being \textit{ultra vires}
  the authority of the provincial legislature), such as Quebec's enacting of
  Bill 21, \textit{Loi sur la laïcité de l'État}.
\item Like in public international law, while there may be a central court
  capable of interpreting the law, the \textit{de facto} relationships between
  the provinces mean that the assumption that its rulings and the constitutional
  law they represent will be obeyed is more grounded in custom and trust than in
  authority and coercion.
\item Refer to van Ert~\cite{van_ert_dubious_2010} for discussion of the
  relationship between international law and Canadian law.
\end{itemize}

\subsection{In Force}
\begin{itemize}
\item Must law be in force to be law?
  \begin{itemize}
  \item The question implies the context of certain objectives for defining law.
  \item Given the objectives of using legal analysis, it is clear that
    hypothetical law, which we will take to be any rules which would be in force
    as law but for the fact that no competent source of law has thus created
    them, may be included in our definition as they may be analyzed under the
    tools and techniques of law as if they were in force.
  \item Furthermore, law which was in force in the past but is no longer is
    obviously a valid subject of legal analysis, and therefore so is law which
    has not yet ever been in force but may at some point in the future.
  \end{itemize}
\item However, it is still useful to formulate the mechanisms of the creation of
  law which is in force.
  \begin{itemize}
  \item It is clear that a person proposing or stating a rule which may, by the
    above definition, be considered a hypothetical rule of law does not cause
    that rule to be in force.
  \item However, it is equally clear that law does not exist as a product of
    nature of the mere existence of its subjects, but rather by the widespread
    recognition (coerced or otherwise) that it has force.
  \item Avoiding such subjective rules is key to minimizing ambiguity and
    opacity in the meaning and validity of legal rules.
  \end{itemize}
\item Therefore, the following axiom is proposed: a legal rule is in force if
  and only if it is either \textit{intra vires} subordinate legislation created
  under the authority of another legal rule, or is customarily accepted by its
  subjects.
\item The disjunctive nature of this axiom is necessary by the recursive nature
  of legal authority in practice: most legal authorities derive their legitimacy
  from other existing legal authorities, but there must be a base authority
  which derives its legitimacy by some other means.
\item A corollary of this axiom is that disregarding any form of customary law
  on the basis of it being customary is illogical, as all law is directly, or
  indirectly based on customary acceptance of legal authority.
\end{itemize}

\section{Evaluating International Law}
\begin{itemize}
\item Public international law will be evaluated under the above definition.
\item Substantively, it does consist of rules binding on people (sovereigns and
  their officials).
\item It is indisputably violable.
\item Much of modern public international law is directly customary, including
  many important concepts and provisions, but a significant amount is
  subordinate legislation in the form of treaties (which derive their authority
  from the customary definition of states and their treaty-making ability).
\item Not only does it match the axiomatic definition, public international law
  also closely resembles the common law in its early forms; reference
  Hasnas~\cite{hasnas_hayek_2004}.
\end{itemize}

\section{Conclusion}
A constructive and axiomatic definition of law provides a maximally general
definition of that which can be analyzed with the tools and techniques of law
and jurisprudence. Such a definition which is not arbitrarily restricted
necessarily concludes that international law as a subject matter is valid law,
and customary international law as a foundational source is a valid means of
constituting law which is in force.

\printbibliography%

\end{document}
